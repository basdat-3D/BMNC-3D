from django.db import models
from fitur_4.models import Polling


# Create your models here.
class PollingBiasa(models.Model):
    id_polling = models.OneToOneField(Polling, models.DO_NOTHING, db_column='id_polling', primary_key=True)
    url = models.CharField(max_length=50)
    deskripsi = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'polling_biasa'
