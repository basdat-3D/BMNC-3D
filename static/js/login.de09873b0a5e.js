$('.form').find('input, textarea').on('keyup blur focus', function (e) {

    var $this = $(this),
        label = $this.prev('label');

    if (e.type === 'keyup') {
        if ($this.val() === '') {
            label.removeClass('active highlight');
        } else {
            label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
        if ($this.val() === '') {
            label.removeClass('active highlight');
        } else {
            label.removeClass('highlight');
        }
    } else if (e.type === 'focus') {

        if ($this.val() === '') {
            label.removeClass('highlight');
        }
        else if ($this.val() !== '') {
            label.addClass('highlight');
        }
    }

});


$('.tab a').on('click', function (e) {

    e.preventDefault();

    $(this).parent().addClass('active');
    $(this).parent().siblings().removeClass('active');

    target = $(this).attr('href');

    $('.tab-content > div').not(target).hide();

    $(target).fadeIn(600);

});

$(document).ready(function () {
    $("#idUniversitasSelect").hide();

    $('#changeRole').on('change', function () {
        var x = document.getElementById("changeRole").value;
        console.log("xxx" + x)
        if (x == 1) {
            console.log("satu");
            $("#status").attr("required", true);
            $("#idUniversitasSelect").hide();
            $("#statusKemahasiswaan").show();
        } else {
            console.log("beda")
            $("#status").removeAttribute("required");
            $("#idUniversitasSelect").show();
            $("#statusKemahasiswaan").hide();
        }
    });
});