from django.db import models
from fitur_3.models import Berita


# Create your models here.
class Polling(models.Model):
    id = models.IntegerField(primary_key=True)
    polling_start = models.DateTimeField()
    polling_end = models.DateTimeField()
    total_responden = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'polling'


class PollingBerita(models.Model):
    id_polling = models.OneToOneField(Polling, models.DO_NOTHING, db_column='id_polling', primary_key=True)
    url_berita = models.ForeignKey(Berita, models.DO_NOTHING, db_column='url_berita')

    class Meta:
        managed = False
        db_table = 'polling_berita'
