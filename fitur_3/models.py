from django.db import models
from fitur_1.models import Universitas
from fitur_1.models import Narasumber

# Create your models here.
class Berita(models.Model):
    url = models.CharField(primary_key=True, max_length=50)
    judul = models.CharField(max_length=100)
    topik = models.CharField(max_length=100)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    jumlah_kata = models.IntegerField()
    rerata_rating = models.FloatField()
    id_universitas = models.ForeignKey(Universitas, models.DO_NOTHING, db_column='id_universitas')

    class Meta:
        managed = False
        db_table = 'berita'


class Tag(models.Model):
    url_berita = models.OneToOneField(Berita, models.DO_NOTHING, db_column='url_berita', primary_key=True)
    tag = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'tag'
        unique_together = (('url_berita', 'tag'),)


class NarasumberBerita(models.Model):
    url_berita = models.OneToOneField(Berita, models.DO_NOTHING, db_column='url_berita', primary_key=True)
    id_narasumber = models.ForeignKey(Narasumber, models.DO_NOTHING, db_column='id_narasumber')

    class Meta:
        managed = False
        db_table = 'narasumber_berita'
        unique_together = (('url_berita', 'id_narasumber'),)
