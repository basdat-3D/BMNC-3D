from django.conf.urls import url
from .views import index, authLogin, validateSignup, signup, authLogout, getUniversitas

app_name = "fitur_1"

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'authlogin/', authLogin, name='authlogin'),
    url(r'signup/', signup, name='signup'),
    url(r'^validateSignup/$', validateSignup, name='validateSignup'),
    url(r'^authlogout/', authLogout, name='authlogout'),
    url(r'^getunivids/', getUniversitas, name='getunivids'),
]
