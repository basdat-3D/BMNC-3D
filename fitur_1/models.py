from django.db import models

# Create your models here.
class Universitas(models.Model):
    id = models.IntegerField(primary_key=True)
    jalan = models.CharField(max_length=99)
    kelurahan = models.CharField(max_length=50)
    provinsi = models.CharField(max_length=50)
    kodepos = models.CharField(max_length=10)
    website = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'universitas'

class Pengguna(models.Model):
    username = models.CharField(primary_key=True, max_length=30)
    password = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pengguna'

class Narasumber(models.Model):
    id = models.IntegerField(primary_key=True)
    username = models.OneToOneField(Pengguna, models.DO_NOTHING, db_column='username', blank=True, null=True, default=None)
    nama = models.CharField(max_length=49)
    email = models.CharField(max_length=50)
    tempat = models.CharField(max_length=50)
    tanggal = models.CharField(max_length=50)
    no_hp = models.CharField(max_length=50)
    jumlah_berita = models.IntegerField()
    rerata_kata = models.IntegerField()
    id_universitas = models.ForeignKey('Universitas', models.DO_NOTHING, db_column='id_universitas')

    class Meta:
        managed = False
        db_table = 'narasumber'

class Mahasiswa(models.Model):
    id_narasumber = models.OneToOneField('Narasumber', models.DO_NOTHING, db_column='id_narasumber', primary_key=True)
    npm = models.CharField(max_length=20)
    status = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'mahasiswa'

class Dosen(models.Model):
    id_narasumber = models.OneToOneField('Narasumber', models.DO_NOTHING, db_column='id_narasumber', primary_key=True)
    nik_dosen = models.CharField(max_length=20)
    jurusan = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'dosen'

class Staf(models.Model):
    id_narasumber = models.OneToOneField(Narasumber, models.DO_NOTHING, db_column='id_narasumber', primary_key=True)
    nik_staf = models.CharField(max_length=20)
    posisi = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'staf'
