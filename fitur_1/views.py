from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.contrib import messages
from django.contrib import admin
from django.views.decorators.csrf import csrf_exempt
from django.db import connection, transaction

from .models import Universitas, Narasumber, Mahasiswa, Staf, Dosen, Pengguna
admin.register(Universitas, Narasumber, Mahasiswa, Staf, Dosen, Pengguna)(admin.ModelAdmin)


response = {}

# Create your views here.
def index(request):
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('fitur_3:index'))
    else:
        html = 'login.html'
        return render(request, html, response)

def authLogin(request):
    print("masuk auth login")
    if request.method == "POST":
        username = request.POST['usernameLogin']
        password = request.POST['passwordLogin']

        cursor = connection.cursor()

        authenticate = "SELECT count(*) FROM pengguna WHERE username='"+username+"' AND password='"+ password +"'"
        cursor.execute(authenticate)
        countUser = int(cursor.fetchone()[0])

        if countUser==1:
            request.session['user_login'] = username
        else:
            messages.error(request, "Username atau password salah", extra_tags="login")

    return HttpResponseRedirect(reverse('fitur_1:index'))


def signup(request):
    print("mausk signup")
    if request.method == "POST":
        role = str(request.POST.get('role', None))
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)
        noId = request.POST.get('noId', None)
        nama = request.POST.get('nama', None)
        tempatLahir = request.POST.get('tempatLahir', None)
        tanggalLahir = request.POST.get('tanggalLahir', None)
        email = request.POST.get('email', None)
        noHp = str(request.POST.get('nohp', None))

        cursor = connection.cursor()

        createUser = "INSERT INTO PENGGUNA(username, password) VALUES('"+ username +"','"+password+"')"
        cursor.execute(createUser)
        transaction.commit()

        countNarasumber = "SELECT count(*) FROM narasumber"
        cursor.execute(countNarasumber)
        idNarasumber = int(cursor.fetchone()[0]) + 1

        randomIdUnivQuery = "SELECT id FROM universitas ORDER BY RANDOM() LIMIT 1"
        cursor.execute(randomIdUnivQuery)
        randomId = int(cursor.fetchone()[0])

        insertNarasumber = "INSERT INTO NARASUMBER(id,nama,email,tempat,tanggal,no_hp,jumlah_berita,rerata_kata,id_universitas,username) "+ \
                               "VALUES (" + str(idNarasumber) + ",'" + nama + "','" + email + "','" + tempatLahir + \
                           "','" + tanggalLahir + "','"+noHp+"',0,0,"+str(randomId)+",'"+username+"')"

        cursor.execute(insertNarasumber)
        transaction.commit()

        print(role=='2')
        if role == '1':
            print("buat mahasiswa")
            status = request.POST.get('status', None)

            insertMahasiswa = "INSERT INTO MAHASISWA(id_narasumber,npm,status) VALUES (" + str(idNarasumber) + ",'"+ \
                               str(noId) + "','" + status + "')"

            cursor.execute(insertMahasiswa)
            transaction.commit()

        elif role == '2':
            print("buat dosen")
            idUniversitas = request.POST.get('idUniversitas', None)
            insertDosen = "INSERT INTO DOSEN(id_narasumber,nik_dosen,jurusan) VALUES (" + str(idNarasumber) + ",'"+ \
                               str(noId) + "','')"

            cursor.execute(insertDosen)
            transaction.commit()

            updateNarasumber = "UPDATE NARASUMBER SET id_universitas = "+str(idUniversitas)+" WHERE id = "\
                               +str(idNarasumber)+""
            cursor.execute(updateNarasumber)
            transaction.commit()

        elif role == '3':
            print("buat staf")
            idUniversitas = request.POST.get('idUniversitas', None)
            insertStaf = "INSERT INTO STAF(id_narasumber,nik_staf,posisi) VALUES (" + str(idNarasumber) + ",'"+ \
                               str(noId) + "','')"

            cursor.execute(insertStaf)
            transaction.commit()

            updateNarasumber = "UPDATE NARASUMBER SET id_universitas = "+str(idUniversitas)+" WHERE id = "\
                               +str(idNarasumber)+""
            cursor.execute(updateNarasumber)
            transaction.commit()

        messages.success(request, "Anda berhasil melakukan sign up. Silakan login", extra_tags="signupSuccess")

        return HttpResponseRedirect(reverse('fitur_1:index'))

@csrf_exempt
def validateSignup(request):
    print("validating signup")
    if request.method == 'POST':

        userName = request.POST.get('username', None)
        userID = int(request.POST.get('noId', None))

        status = {}

        cursor = connection.cursor()

        checkMahasiswa = "SELECT count(*) FROM mahasiswa WHERE npm='"+str(userID)+"'"
        cursor.execute(checkMahasiswa)
        countMahasiswa = int(cursor.fetchone()[0])
        print("mahasiswa " + str(countMahasiswa))

        checkDosen = "SELECT count(*) FROM dosen WHERE nik_dosen='"+str(userID)+"'"
        cursor.execute(checkDosen)
        countDosen = int(cursor.fetchone()[0])
        print("dosen " + str(countDosen))

        checkStaf = "SELECT count(*) FROM staf WHERE nik_staf='"+str(userID)+"'"
        cursor.execute(checkStaf)
        countStaf = int(cursor.fetchone()[0])
        print("staf " + str(countStaf))

        if (countMahasiswa!=0 or countDosen!=0 or countStaf!=0):
            status["idIsExists"] = "True"

        checkUser = "SELECT count(*) FROM PENGGUNA WHERE username='"+userName+"'"
        cursor.execute(checkUser)
        countUser = int(cursor.fetchone()[0])

        if countUser!=0:
            status["usernameIsExists"] = "True"

        print(status)
        return JsonResponse(status, content_type='application/json')


def authLogout(request):
    request.session.flush() # menghapus semua session
    messages.success(request, "Anda berhasil logout", extra_tags="logoutSuccess")
    print("logout")
    return HttpResponseRedirect(reverse('fitur_1:index'))

def getUniversitas(request):
    if request.method == 'GET':
        universitas = Universitas.objects.raw("SELECT id FROM universitas")

        ids = []
        for univ in universitas:
            ids.append(univ.id)

        return JsonResponse(ids, safe=False)