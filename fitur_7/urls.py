from django.conf.urls import url
from .views import index

app_name = "fitur_7"

urlpatterns = [
    url(r'^$', index, name='index')
]
