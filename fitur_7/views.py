from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from fitur_3.models import *

# Create your views here.
def index(request):
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect('/login/')
    else:
        response = {}
        html = 'fitur_7.html'
        return render(request, html, response)
