"""bmnc3d URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.views.generic import RedirectView
from django.contrib import admin
# from django.urls import path
import fitur_1.urls as fitur_1
import fitur_3.urls as fitur_3
import fitur_4.urls as fitur_4
import fitur_5.urls as fitur_5
import fitur_6.urls as fitur_6
import fitur_7.urls as fitur_7
import fitur_8.urls as fitur_8

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', include(fitur_1, namespace='fitur_1')),
    url(r'^$', RedirectView.as_view(url='login/', permanent=True)),
    url(r'^buat_berita/', include(fitur_3, namespace='fitur_3')),
    url(r'^polling_berita', include(fitur_4, namespace='fitur_4')),
    url(r'^polling_biasa', include(fitur_5, namespace='fitur_5')),
    url(r'^profile', include(fitur_6, namespace='fitur_6')),
    url(r'^lihat_berita', include(fitur_7, namespace='fitur_7')),
    url(r'^lihat_polling', include(fitur_8, namespace='fitur_8')),
]
